About Models4Insight
====================

Models4Insight is a Software as a Service solution, that exist of 3
parts: Model repository, Platform and Portal.

With the Model repository you can manage the lifecycle of your ArchiMate
models. By storing your models in the Models4Insight **model
repository**, you create a single source of truth which can be used for
analysis. The repository is accessible via the Models4Insight
**platform**, which also provides model management features, such as:
Versioning, Conflict management and Branching.

The Models4Insight **portal** gives functionalities for publishing,
visualizing and analysing to deliver benefits to all your stakeholders.
By combining your models with your data, it becomes possible to derive
new and unique insights and intelligence.

This user guide is intended as a reference to help you get set up with
Models4Insight quickly. We explain how to work with the model repository
through the platform, and how to visualize your models via the portal.

Definitions
===========

Archi
-----

Archi is a free to use, open source editor for ArchiMate models. It can
be downloaded on
`www.archimatetool.com <file:///C:\Users\Marlon%20Hiralal\AppData\Local\Temp\Temp1_User%20Guide%20V1.zip\www.archimatetool.com>`__.

Branch
------

A branch is a separate version of the model that you can edit in a
vacuum. The initial model uploaded within the project is saved as the
master branch. You have the option of cloning branches and merging them
again.

Clone branch
------------

Cloning a branch means creating an identical copy of your model that can
be edited in isolation.

Color view
----------

A color view is a visualization based on your model that uses colors to
highlight specific properties, such as data associated with the concepts
or differences between two states of the model. Models4Insight uses
color views as part of the *Model explorer dashboard*.

Concept data
------------

Concept data relates to properties associated with elements in the
model. Models4Insight combines models and data to help you gain new
insights. Data related to your model is available via the *Model
explorer dashboard*.

Merge branch 
-------------

If your project has two or more branches, you have the option to merge
two of them into a single branch. Merging combines the changes from each
branch into a new version of your model.

Model navigator
---------------

This navigation tree can be used to select which views you want to
include in the color view\ **.** The structure of the tree mimics the
structure of your model. The model navigator is explained in more detail
under *Model navigator*.

Project member
--------------

A project member is a user who has access to a project. This includes
the creator of the project and any people they have granted project
membership to. See section *Add member* on how to add people to your
project.

Publish model
-------------

By publishing a model\ **,** you release the current, as well as any
future versions of your models to the Models4Insight portal. You have
the option to unpublish your model at any time. To read more about
publishing your model, please see the section on *Portal settings*.

Retrieve model
--------------

You can use the project homepage on the platform to retrieve a version
of your model as a file. These files can be used with the Archi editor
to make changes to your model. To read more, please see *Retrieve
model*.

Timeline filter
---------------

You can select which versions of your model you would like to see in the
model explorer dashboard by timestamp and by branch via the timeline
filter. To read more, please refer to *Timeline filter*.

Tracking information
--------------------

Any model retrieved from the platform has tracking information
associated with it. The tracking information helps us to keep track of
the changes you and your team make to the model. You can view this
information in Archi under the properties section. **Do not change these
properties.**

Unpublish model
---------------

Unpublishing makes the dashboards for your project in the portal
unavailable until you re-publish your project. Read more on how to
unpublish your project under *Portal settings*.

Upload model 
-------------

You can upload new versions of your model to the repository via the
project homepage. To read more, please review Upload model.

Common Issues
=============

Registration
------------

If you are having trouble registering an account, please make sure that
your username is at least three characters long, is lower case, consists
of a-z, \_, and numbers 0-9.

You can only create one username per e-mail. If your e-mail is already
being used for another account on Models4Insight, it will not allow you
to register again.

If you are not receiving the registration e-mail and think you may have
misspelled your email, please check if you correctly spelled your e-mail
by simply attempting to register again using the same e-mail. If the
registration works then you misspelled it the first time, if it gives an
error then try refreshing your e-mail. As long as your account is not
verified, you will have the option of resending the verification email
when you log in.

 Models4Insight Platform
------------------------

**For your first commit**, please make sure to upload a model that is
not already being used for another project (so no tracking information
is present).

**If you are having trouble uploading a model, make sure that:**

-  The comment section is not empty

-  The file is not already being used for another project (so no
   tracking information is present)

-  Your model is using the .archimate extension

**If you are having trouble cloning a branch, make sure that:**

-  The name of the new branch is not empty and unique for project. The
   name should also not contain any special characters.

-  The comment section is not empty

**If you are having trouble merging a branch, make sure that:**

-  The name of the target branch is specified

-  The comment section is not empty

**If you are having trouble adding new people to your project**, please
make sure their username/e-mail is correctly spelled

Models4Insight Portal
---------------------

At the moment, users added to your project in the portal do not
automatically get access to your dashboard in the portal. This will be
added in the future.

.. _registration-1:

Registration
============

To get started go to
`www.models4insight.com <http://www.models4insight.com>`__ and click on
the **register** button.

|landingpage|

Clicking the register button takes you to the user registration page
where you are asked to provide some personal information. **Note: A
username cannot use capital letters, special characters or double
spaces.**

|register|

Once you have filled in all the information, click on register. You will
receive a verification email at the address you provided. You need to
verify your registration before you can log in.

You should immediately receive an email from Aurelius Enterprise. The
link within is only valid for 5 minutes. Check your spam folder if you
cannot find the email. If 5 minutes has passed, please click on the
re-send email button.

Login
=====

The Models4Insight platform and portal use a single sign-on. This means
you only need to log in once to use either. To log in, press the login
button at the top of the Models4Insight homepage.

|landingpage|

Once logged in, you will be directed to the Models4Insight platform.

Accessing the Models4Insight Platform
=====================================

You can also access the platform by clicking on the **platform** button
on the Models4Insight homepage.

The platform is where you upload your models, do version control, and
control the authorization. You can upload different versions, retrieve
the versions that you previously uploaded, clone branches to work
simultaneously on different versions of the same model and merge them
back together.

Projects Overview
=================

On opening the Models4Insight portal, you land on the **projects
overview** page. This page looks as follows:

|platformhomepage|

Here, you can create a new project or access existing projects of which
you are a member.

Create project
--------------

To create a project, fill in a **project name** and **description**. The
project name must be unique for the user, must contain at least three
characters, cannot contain special characters or double spaces.
Additionally, the description of the project cannot be empty. Under
**subscription,** you have an option to make this project **public** or
**private**. When you decide to make a project public, anyone who has
access to the platform can see it. Creating a private project limits
access to you and those you have authorized (how to add new members to
your project is explained under the Add member section). Select your
preference and click the **create** button.

Opening an existing project
---------------------------

In addition to creating projects, you can also open your previously
created projects or any projects shared with you. The table on the right
of the page lists all projects of which you are a member. You can open
any project by clicking its name in the table.

Project Homepage
================

Opening a project redirects you to that project’s homepage. The project
homepage is where you control your project.

Project actions
---------------

There are various actions you can perform related to version control,
tracking your team’s activities, access control and publishing your
model(s) to the portal. Each of these actions is described in the
sections below.

.. _upload-model-1:

Upload model
~~~~~~~~~~~~

You can upload your ArchiMate model by clicking **Choose file** and
selecting a model from your computer. The platform currently supports
models created in the open-source tool Archi
(`www.archimatetool.com) <http://www.archimatetool.com)>`__. You can
recognize Archi models by the .\ **archimate** extension. After filling
in the **Comment** section (this cannot be empty!) click on **Upload
file**. When you upload a model, it becomes available in the model
repository. Additionally, you will automatically receive a new file
containing your model with tracking information.

Your first commit
~~~~~~~~~~~~~~~~~

For your first commit, it is important to upload a model that was not
previously part of another project on the platform. Tracking information
is automatically associated with models on the platform. This tracking
info is unique for a project. Adding a tracked model to a new project
fails as a result.

|uploadfile|

.. _retrieve-model-1:

Retrieve model
~~~~~~~~~~~~~~

To retrieve a model from your project, click on **Retrieve File** and
select whichever version you would like to retrieve. If you do not
specify a version, you retrieve the latest version by default. You can
use Archi to open these models. The name of the file you retrieve is
automatically generated. Do not be confused if this is different from
the original name of the file you uploaded.

.. _clone-branch-1:

Clone branch
~~~~~~~~~~~~

If you want to isolate a version of your model to make adjustments
without affecting the original, you have the option of cloning that
version and creating a new branch. Branching creates a separate version
of the model that you can edit in a vacuum. The initial model uploaded
within the project is saved as the master branch. To clone a branch,
click on **Clone branch** and specify the branch you want to clone in
the upper drop-down menu. Fill in the name of this new branch which must
be unique for the project. The comment section cannot be empty.

Note: In this example, the branch cloned is the master branch. However,
you can clone any branch (including branches of branches) any number of
times.

|clonebranch|

.. _merge-branch-1:

Merge branch
~~~~~~~~~~~~

If your project has two or more branches, you have the option to merge
two of them into a single branch. Merging combines the changes from each
branch into a new version of your model. Start by clicking on **Merge
branch**. The name of the target branch (the branch to which the merge
is applied) must be different from your current branch (specified with
the select box at the top of the page). You can use the comment section
to describe the purpose of the merge. This section cannot be empty.

Conflict resolution
^^^^^^^^^^^^^^^^^^^

If two branches contain a change to the same model element, a conflict
arises when you attempt to merge the two versions. If this happens, a
pop-up window appears asking you to reconcile the conflict(s). For each
conflict, you have the option to apply the change from one version. The
changes you did not apply are subsequently ignored.

|mergebranches|

Project description 
~~~~~~~~~~~~~~~~~~~~

To edit the project description, click on the little pencil below the
project name at any time and edit this. You cannot leave the description
empty.

|projectdescription|

Portal settings
~~~~~~~~~~~~~~~

Under the portal settings, you can control whether you publish your
models and data to the portal. By selecting **Publish Models,** you
publish the current, as well as any future versions of your models. Once
you have checked the box, a link appears on the right of the checkbox.
Clicking this **link** takes you to your dashboard on the portal. You
can disable publishing by unchecking the Publish Models option.
Unpublishing also makes the dashboards in the portal unavailable until
you re-publish your project.

|publishportal|

Project settings
---------------

Add member
~~~~~~~~~~

The **Members** function below settings allows you to give other users
access to your project. Members of a project can upload and retrieve
models, and clone and merge branches. Only the creator of the project
can access the project settings. Provide the username of each member you
wish to add to the project. This username needs to be valid.

|addmembers|


.. _models4insight-portal-1:

Models4Insight Portal
=====================

The portal is used for the visualization of your models and data. Here,
you can look at models, view specifics about the model, view whole
models or parts of them. There is a default visualization generated for
you to start with but you can create new visualization if you wish. In
the portal, you can further investigate your models to develop a better
understanding of the data behind it. This is where the link between the
data and the models itself is visualized.

Model explorer dashboard
------------------------

The model explorer dashboard displays the published models as shown
below:

|image10|

.. _color-view-1:

Color view
----------

In the first drop-down menu, you can select which model you would like
to see. By selecting a value in the second drop-down menu, you can
compare another model to the one selected in the first drop-down menu.
The comparison applies colors to the model. When the outline of the
model is green, it is present in both the selected model and the model
you are comparing. When the outline is red, it is present in the
currently displayed model only.

.. _concept-data-1:

Concept data
------------

Whenever you click on a concept in the model, the concept data section
shows its associated data. The selected concept receives green outline.
To unselect, press the concept again. You can also select the relations.
If no data is available for the selected concept, a ‘no data’ message is
displayed instead.

.. _timeline-filter-1:

Timeline filter
---------------

You can select which versions you would like to see by timestamp and by
branch. Select a value in the respective drop-down menus to apply each
filter. You can select more than one value. To remove a value from the
filter, click the **x** on the left of a selected value.

.. _model-navigator-1:

Model navigator
---------------

This navigation tree can be used to select which views you want to
include in the **color view.** The structure of the tree mimics the
structure of your model. You can choose which views to display by
clicking on the orange boxes. Once selected the box turns green. You can
deselect by clicking it again. You can select multiple by holding CTRL
and clicking multiple boxes. Unselect the entire previous selection by
selecting another box without holding CTRL. You can collapse and expand
parts of the tree by clicking on the blue boxes.

.. |image0| image:: media/image2.png
   :width: 6.26806in
   :height: 3.70069in
.. |image1| image:: media/image3.png
   :width: 6.26806in
   :height: 3.70625in
.. |image2| image:: media/image2.png
   :width: 6.26806in
   :height: 3.70069in
.. |image3| image:: media/image4.png
   :width: 6.26806in
   :height: 3.46806in
.. |image4| image:: media/image5.png
   :width: 6.26806in
   :height: 3.90486in
.. |image5| image:: media/image6.png
   :width: 6.26806in
   :height: 2.35278in
.. |image6| image:: media/image7.png
   :width: 6.26806in
   :height: 2.68333in
.. |image8| image:: media/image9.png
   :width: 6.26806in
   :height: 3.08958in
.. |image9| image:: media/image10.png
   :width: 6.26806in
   :height: 2.96597in
.. |image10| image:: media/image13.png
   :width: 6.26806in
   :height: 2.2in
.. |landingpage| image:: media/landingpage.png
   :width: 6.26806in
   :height: 2.2in
.. |addmembers| image:: media/addmembers.png
   :width: 6.26806in
   :height: 2.2in
.. |clonebranch| image:: media/clonebranch.png
   :width: 6.26806in
   :height: 2.2in
.. |login| image:: media/login.png
   :width: 6.26806in
   :height: 2.2in
.. |mergebranches| image:: media/mergebranches.png
   :width: 6.26806in
   :height: 2.2in
.. |platformhomepage| image:: media/platformhomepage.png
   :width: 6.26806in
   :height: 2.2in
.. |projectdescription| image:: media/projectdescription.png
   :width: 1.79079in
   :height: 0.82393in
.. |publishportal| image:: media/publishportal.png
   :width: 6.26806in
   :height: 2.2in
.. |register| image:: media/register.png
   :width: 6.26806in
   :height: 2.2in
.. |uploadfile| image:: media/uploadfile.png
   :width: 6.26806in
   :height: 2.2in
